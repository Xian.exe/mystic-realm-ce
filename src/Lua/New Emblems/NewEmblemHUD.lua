freeslot("SPR_UNCEMB")
local emblemlist = {}
local offset = 320
local offsetresettimer = 0
local tabresettimer = 2
local touchedresettimer = 25
local tabMenuOpened = false

addHook("PlayerSpawn", function()
	emblemlist = {}
	for mapemblem in mobjs.iterate()
		if mapemblem.valid and mapemblem.type == MT_EMBLEM
			local emblem = {frame = mapemblem.frame, color = mapemblem.color, origmobj = mapemblem}
			table.insert(emblemlist, emblem)
		end
	end
end)

addHook("TouchSpecial", function(emblem, pmo)
	pmo.player.changeoffset = 1
	offsetresettimer = touchedresettimer
end, MT_EMBLEM)

addHook("ThinkFrame", function()
	for p in players.iterate
		if not p.bot and p.valid
			if tabMenuOpened == true
				p.changeoffset = 1
				offsetresettimer = 2
				tabMenuOpened = false
			end
			if p.changeoffset
				if offset > 160
					offset = $ - 10
				end
				if offset == 160 and offsetresettimer
					offsetresettimer = $ - 1
				end
				if not offsetresettimer
					p.changeoffset = 0
				end
			else
				if offset < 320
					offset = $ + 20
				end
			end
		end
	end
end)

hud.add(function(d, p)
	local loffset = offset
	local bg = d.cachePatch("EMBBG")
	d.draw(loffset, 19, bg, V_SNAPTORIGHT|V_SNAPTOTOP)
	for embnum, emblem in ipairs(emblemlist)
		local emblempatch = d.getSpritePatch("EMBM", emblem.frame & FF_FRAMEMASK)
		local uncollectedemb = d.cachePatch("UNCEMB")
		local emblemcolor = d.getColormap(TC_DEFAULT, emblem.color)
		//d.drawFill((loffset + emblempatch.width/4), 45, 6969, 6, 139|V_SNAPTORIGHT)
		d.drawScaled((loffset*FRACUNIT + ((uncollectedemb.width / 4)*FRACUNIT)), 24*FRACUNIT, FRACUNIT/2, uncollectedemb, V_SNAPTORIGHT|V_SNAPTOTOP, emblemcolor)
		if (emblem.frame & FF_TRANSMASK) or (not emblem.origmobj.valid)
			d.drawScaled((loffset*FRACUNIT + ((emblempatch.width / 4)*FRACUNIT)), 24*FRACUNIT, FRACUNIT/2, emblempatch, V_SNAPTORIGHT|V_SNAPTOTOP, emblemcolor)
		end
		loffset = $ + 32
	end
end, "game")

hud.add(function(d, p)
	tabMenuOpened = true
	offsetresettimer = tabresettimer
end, "scores")